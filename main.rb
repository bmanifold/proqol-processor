#!/usr/bin/env ruby

# This is a sample file showing how this proqol post processor could be used

require './results'

rp = ResultsProcessor.new('sample_data.csv', 2)
rp.load
rp.print_results

# If you would like to have the results written out to a new csv file uncomment the lines below
#filename = Time.new.strftime('%y%m%d-%H%M%S-proqol-scores.csv')
#rp.write_results(filename)
