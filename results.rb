require 'csv'
require './proqol'

class ResultsProcessor
  attr_accessor :raw_data, :filename, :headers, :pq_processor

  def initialize(filename, index)
    self.filename = filename
    self.headers = true
    self.pq_processor = ProqolProcessor.new(index)
  end

  def load()
    result_values = {"Never" => 1, "Rarely" => 2, "Sometimes" => 3, "Often" => 4, "Very Often" => 5}
    proqol_converter = lambda do |field, info| 
      if info.index >= pq_processor.start_index && info.index <= pq_processor.stop_index
        result_values[field]
      else
        field
      end
    end
    self.raw_data = CSV.read(filename, headers: headers, converters: [proqol_converter])
  end

  def process()
    self.raw_data.map {|row| pq_processor.process(row)}
  end

  def print_results()
    process().each do |result|
      puts result.join(",")
    end
  end

  def write_results(filename="proqol_results.csv")
    CSV.open(filename, 'w') do |csv|
      csv << raw_data.headers + ["compassion", "burnout", "trauma"]
      process.each do |row|
        csv << row
      end
    end
  end
end
