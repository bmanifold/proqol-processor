# This class is used to score a Proqol assessment
# It expects that the proqol answers are contiguous and in the order
# shown on the assessment listed here:
# https://www.proqol.org/uploads/ProQOL_5_English.pdf

class ProqolProcessor
  attr_accessor :start_index, :stop_index

  def initialize(start_index)
    self.start_index = start_index
    self.stop_index = start_index + 29
  end

  # The row being passed in is expected to be a CSV::Row
  def process(row)
    compassion = score_compassion(row)
    burnout = score_burnout(row)
    trauma = score_trauma(row)
    return row.fields + [compassion, burnout, trauma]
  end

  def score_compassion(row)
    questions = [3,6,12,16,18,20,22,24,27,30]
    answers = row[start_index..stop_index]
    questions.reduce(0) do |sum, q|
      sum += answers[q-1]
    end
  end

  def score_burnout(row)
    questions = [1,4,8,10,15,17,19,21,26,29]
    reverse_scored = [1,4,15,17,29]
    answers = row[start_index..stop_index]
    questions.reduce(0) do |sum, q|
      ans = answers[q-1]
      if reverse_scored.include?(q)
        ans = 6 - ans
      end
      sum += ans
    end
  end

  def score_trauma(row)
    questions = [2,5,7,9,11,13,14,23,25,28]
    answers = row[start_index..stop_index]
    questions.reduce(0) do |sum, q|
      sum += answers[q-1]
    end
  end
end
