# ProQOL Results Post Processor

The purpose of this code is to help score multiple ProQOL assements.

The code was written based on the ProQOL assessment here: [ProQOL 5](https://www.proqol.org/uploads/ProQOL_5_English.pdf)

It was also written with the assessment answers being words and not numbers, i.e. `Never`, `Rarely`, `Sometimes`, `Often`, `Very Often` (please refer to the `sample_data.csv` for an example)


## Getting started

* The only dependency needed is Ruby.
  * This code was tested against Ruby 2.7, but should in theory work fine with most other 2.x versions.
  * It should also be noted that this code was only tested on Linux and OS X.
* After installing Ruby make sure you are in same directory as this README.md file and run the following:
  ```
  ./main.rb
  ```
  Running `main.rb` will use the `sample_data.csv` file as input and print the results out to the screen.
  If you would like the results to be printed to a file, open the `main.rb` in a text editor and uncomment the last two lines.
  The `main.rb` file is only intended as an example of how the code in `results.rb` and `proqol.rb` could be used, but should also be easy enough modify and use as is if needed.
